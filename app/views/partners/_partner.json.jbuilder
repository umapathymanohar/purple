json.extract! partner, :id, :name, :type, :pan_no, :aadhaar_no, :voter_id_no, :mobile_no, :alternate_mobile_no, :date_of_birth, :bank_name, :bank_ac_no, :bank_branch, :customer_id, :created_at, :updated_at
json.url partner_url(partner, format: :json)
