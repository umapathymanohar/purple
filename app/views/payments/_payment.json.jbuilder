json.extract! payment, :id, :subscription_id, :amount, :date_paid, :created_at, :updated_at
json.url payment_url(payment, format: :json)
