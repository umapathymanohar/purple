json.extract! customer, :id, :name, :code, :address, :gst_number, :pan_number, :aadhaar_number, :voter_id_number, :mobile_number, :alternate_mobile_number, :date_of_birth, :bank_name, :bank_account_number, :bank_branch, :service_id, :created_at, :updated_at
json.url customer_url(customer, format: :json)
