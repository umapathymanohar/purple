json.extract! subscription, :id, :customer_id, :service_id, :subscription_start, :subscription_end, :created_at, :updated_at
json.url subscription_url(subscription, format: :json)
