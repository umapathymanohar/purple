json.extract! service, :id, :name, :credits_per_month, :price_per_year, :created_at, :updated_at
json.url service_url(service, format: :json)
