require 'test_helper'

class PartnersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @partner = partners(:one)
  end

  test "should get index" do
    get partners_url
    assert_response :success
  end

  test "should get new" do
    get new_partner_url
    assert_response :success
  end

  test "should create partner" do
    assert_difference('Partner.count') do
      post partners_url, params: { partner: { aadhaar_no: @partner.aadhaar_no, alternate_mobile_no: @partner.alternate_mobile_no, bank_ac_no: @partner.bank_ac_no, bank_branch: @partner.bank_branch, bank_name: @partner.bank_name, customer_id: @partner.customer_id, date_of_birth: @partner.date_of_birth, mobile_no: @partner.mobile_no, name: @partner.name, pan_no: @partner.pan_no, type: @partner.type, voter_id_no: @partner.voter_id_no } }
    end

    assert_redirected_to partner_url(Partner.last)
  end

  test "should show partner" do
    get partner_url(@partner)
    assert_response :success
  end

  test "should get edit" do
    get edit_partner_url(@partner)
    assert_response :success
  end

  test "should update partner" do
    patch partner_url(@partner), params: { partner: { aadhaar_no: @partner.aadhaar_no, alternate_mobile_no: @partner.alternate_mobile_no, bank_ac_no: @partner.bank_ac_no, bank_branch: @partner.bank_branch, bank_name: @partner.bank_name, customer_id: @partner.customer_id, date_of_birth: @partner.date_of_birth, mobile_no: @partner.mobile_no, name: @partner.name, pan_no: @partner.pan_no, type: @partner.type, voter_id_no: @partner.voter_id_no } }
    assert_redirected_to partner_url(@partner)
  end

  test "should destroy partner" do
    assert_difference('Partner.count', -1) do
      delete partner_url(@partner)
    end

    assert_redirected_to partners_url
  end
end
