require 'test_helper'

class CustomersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @customer = customers(:one)
  end

  test "should get index" do
    get customers_url
    assert_response :success
  end

  test "should get new" do
    get new_customer_url
    assert_response :success
  end

  test "should create customer" do
    assert_difference('Customer.count') do
      post customers_url, params: { customer: { aadhaar_number: @customer.aadhaar_number, address: @customer.address, alternate_mobile_number: @customer.alternate_mobile_number, bank_account_number: @customer.bank_account_number, bank_branch: @customer.bank_branch, bank_name: @customer.bank_name, code: @customer.code, date_of_birth: @customer.date_of_birth, gst_number: @customer.gst_number, mobile_number: @customer.mobile_number, name: @customer.name, pan_number: @customer.pan_number, service_id: @customer.service_id, voter_id_number: @customer.voter_id_number } }
    end

    assert_redirected_to customer_url(Customer.last)
  end

  test "should show customer" do
    get customer_url(@customer)
    assert_response :success
  end

  test "should get edit" do
    get edit_customer_url(@customer)
    assert_response :success
  end

  test "should update customer" do
    patch customer_url(@customer), params: { customer: { aadhaar_number: @customer.aadhaar_number, address: @customer.address, alternate_mobile_number: @customer.alternate_mobile_number, bank_account_number: @customer.bank_account_number, bank_branch: @customer.bank_branch, bank_name: @customer.bank_name, code: @customer.code, date_of_birth: @customer.date_of_birth, gst_number: @customer.gst_number, mobile_number: @customer.mobile_number, name: @customer.name, pan_number: @customer.pan_number, service_id: @customer.service_id, voter_id_number: @customer.voter_id_number } }
    assert_redirected_to customer_url(@customer)
  end

  test "should destroy customer" do
    assert_difference('Customer.count', -1) do
      delete customer_url(@customer)
    end

    assert_redirected_to customers_url
  end
end
