Rails.application.routes.draw do
  resources :payments
  resources :subscriptions
  resources :services
  resources :customers
  resources :customer_services
  resources :partners
 
    
 devise_for :users, controllers: { registrations: "users/registrations", sessions: "users/sessions", passwords: "users/passwords", confirmations: "users/confirmations" }
 
 

  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
