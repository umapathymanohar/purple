class DeleteTablePayments < ActiveRecord::Migration[5.1]
  def up
    drop_table :payments
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
