class RemoveColumnFromPartners < ActiveRecord::Migration[5.1]
 
  def up
    remove_column :partners, :code, :string
  end

  def down
    add_column :partners, :code, :string
  end
end
