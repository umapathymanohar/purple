class AddCustomerIdToPartners < ActiveRecord::Migration[5.1]
  def change
    add_column :partners, :customer_id, :integer
  end
end
