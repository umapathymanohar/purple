class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :code
      t.text :address
      t.string :gst_number
      t.string :pan_number
      t.string :aadhaar_number
      t.string :voter_id_number
      t.string :mobile_number
      t.string :alternate_mobile_number
      t.date :date_of_birth
      t.string :bank_name
      t.string :bank_account_number
      t.string :bank_branch
      t.integer :service_id

      t.timestamps
    end
  end
end
