class AddDateOfBirthToPartners < ActiveRecord::Migration[5.1]
  def change
    add_column :partners, :date_of_birth, :date
  end
end
