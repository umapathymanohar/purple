class CreateSubscriptions < ActiveRecord::Migration[5.1]
  def change
    create_table :subscriptions do |t|
      t.integer :customer_id
      t.integer :service_id
      t.date :subscription_start
      t.date :subscription_end

      t.timestamps
    end
  end
end
