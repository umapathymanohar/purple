class CreatePartners < ActiveRecord::Migration[5.1]
create_table :partners do |t|
      t.string :name
      t.text :address
      t.string :code
      t.string :gst_number
      t.string :pan_number
      t.string :aadhaar_number
      t.string :voter_id_number
      t.string :mobile_number
      t.string :alternate_mobile_number
      t.string :bank_name
      t.string :bank_account_number
      t.string :bank_branch
      t.string :customer_type
      t.timestamps
    end
end
