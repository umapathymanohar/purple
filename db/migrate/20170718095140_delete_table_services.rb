class DeleteTableServices < ActiveRecord::Migration[5.1]
  def up
    drop_table :services
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
