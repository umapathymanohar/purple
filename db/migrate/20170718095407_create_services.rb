class CreateServices < ActiveRecord::Migration[5.1]
  def change
    create_table :services do |t|
      t.string :name
      t.decimal :credits_per_month
      t.decimal :price_per_year

      t.timestamps
    end
  end
end
