class RemoveColumnFromCustomers < ActiveRecord::Migration[5.1]
  def up
    remove_column :customers, :service_id, :integer
  end

  def down
    add_column :customers, :service_id, :integer
  end
end
