class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.integer :subscription_id
      t.decimal :amount
      t.date :date_paid

      t.timestamps
    end
  end
end
